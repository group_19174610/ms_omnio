package az.atl.ms_auth.service;

import az.atl.ms_auth.feign.MessageFeignClient;
import az.atl.ms_auth.model.consts.Channel;
import az.atl.ms_auth.model.dto.message.AdminMessageDto;
import az.atl.ms_auth.model.dto.message.MessageDto;
import az.atl.ms_auth.model.dto.message.MessageSendRequest;
import az.atl.ms_auth.service.impl.MessageServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class MessageServiceImplTest {
    @Mock
    MessageFeignClient client;

    private MessageServiceImpl messageService;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        messageService=new MessageServiceImpl(client);
    }


    @Test
    void sendMessage() {
        //Given
        MessageSendRequest request=new MessageSendRequest("receiverUsername","messageBody",
                Channel.CHANNEL_EMAIL);
        String authorization="authHeader";
        MessageDto exceptedResponse=new MessageDto("senderName","receiverName","messageBody",
                true,Channel.CHANNEL_EMAIL,LocalDateTime.now());

        //When
        when(messageService.sendMessage(request,authorization)).thenReturn(exceptedResponse);

        //Then
        assertAll("Message send successfully",
                ()->{
                    assertDoesNotThrow(()->messageService.sendMessage(request,authorization));
                }
        );
    }

    @Test
    void getMyMessages() {
        // Given
        String authorization = "authHeader";
        List<MessageDto> expectedResponse = new ArrayList<>();
        // When
        when(client.getMyMessages(authorization)).thenReturn(expectedResponse);

        // Then
        List<MessageDto> actualResponse = messageService.getMyMessages(authorization);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void getMyDialogue() {
        // Given
        String otherUserName = "otherUserName";
        String authorization = "authHeader";
        List<MessageDto> expectedResponse = new ArrayList<>(); // populate this with your expected messages

        // When
        when(client.getMyDialogue(otherUserName, authorization)).thenReturn(expectedResponse);

        // Then
        List<MessageDto> actualResponse = messageService.getMyDialogue(otherUserName, authorization);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void getMessages() {
        // Given
        String senderName = "senderName";
        String receiverName = "receiverName";
        String authorization = "authHeader";
        List<AdminMessageDto> expectedResponse = new ArrayList<>();
        // When
        when(client.getMessages(senderName, receiverName, authorization)).thenReturn(expectedResponse);

        // Then
        List<AdminMessageDto> actualResponse = messageService.getMessages(senderName, receiverName, authorization);
        assertEquals(expectedResponse, actualResponse);
    }
}