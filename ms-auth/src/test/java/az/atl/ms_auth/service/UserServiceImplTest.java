package az.atl.ms_auth.service;

import az.atl.ms_auth.dao.entity.UserEntity;
import az.atl.ms_auth.dao.repository.UserRepository;
import az.atl.ms_auth.exception.IncorrectPasswordException;
import az.atl.ms_auth.exception.PasswordMismatchException;
import az.atl.ms_auth.exception.UserNameNotFoundException;
import az.atl.ms_auth.exception.UserNotFoundException;
import az.atl.ms_auth.model.Role;
import az.atl.ms_auth.model.UpdatePasswordRequest;
import az.atl.ms_auth.model.UpdateUserRequest;
import az.atl.ms_auth.model.dto.UserDto;
import az.atl.ms_auth.service.impl.UserServiceImpl;
import jakarta.persistence.Embeddable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserDetails userDetails;

    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        userService = new UserServiceImpl(passwordEncoder, userRepository);
    }

    @Test
    void updatePassword_Success() {
        //Given
        String userName = "userName";
        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest("oldPassword",
                "newPassword", "newPassword");

        userDetails = new User(userName, updatePasswordRequest.getOldPassword(), Collections.emptyList());
        UserEntity user = new UserEntity();
        user.setUserName(userName);
        user.setPassword(passwordEncoder.encode(updatePasswordRequest.getOldPassword()));

        when(userRepository.findByUserName(userName)).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(updatePasswordRequest.getOldPassword(), user.getPassword())).thenReturn(true);

        //When and Then
        assertAll("Password Update Success",
                () -> {
                    assertDoesNotThrow(() -> userService.updatePassword(updatePasswordRequest, userDetails));
                    verify(userRepository, times(1)).findByUserName(userName);
                    verify(passwordEncoder, times(1)).matches(updatePasswordRequest.getOldPassword(), user.getPassword());
                    verify(userRepository, times(1)).save(user);
                }
        );
    }

    @Test
    void updatePassword_PasswordMisMatch() {
        //Given
        String userName = "userName";
        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest("oldPassword",
                "newPassword1", "newPassword2");

        userDetails = new User(userName, updatePasswordRequest.getOldPassword(), Collections.emptyList());

        //When and Then
        assertThrows(PasswordMismatchException.class, () -> userService.updatePassword(updatePasswordRequest, userDetails));
    }

    @Test
    void updatePassword_UserNameNotFound() {
        //Given
        String userName = "userName";
        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest("oldPassword",
                "newPassword", "newPassword");

        userDetails = new User(userName, updatePasswordRequest.getOldPassword(), Collections.emptyList());

        when(userRepository.findByUserName(userName)).thenReturn(Optional.empty());
        //When and Then
        assertThrows(UserNameNotFoundException.class, () -> userService.updatePassword(updatePasswordRequest, userDetails));
    }

    @Test
    void updatePassword_IncorrectPassword() {
        //Given
        String userName = "userName";
        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest("oldPassword",
                "newPassword", "newPassword");

        userDetails = new User(userName, updatePasswordRequest.getOldPassword(), Collections.emptyList());

        UserEntity user = new UserEntity();
        user.setUserName(userName);
        user.setPassword("differentOldPassword");

        when(userRepository.findByUserName(userName)).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(updatePasswordRequest.getOldPassword(), user.getPassword())).thenReturn(false);
        //When and Then
        assertThrows(IncorrectPasswordException.class, () -> userService.updatePassword(updatePasswordRequest, userDetails));
    }

    @Test
    void getUserById_Success() {
        //Given
        UserEntity user = new UserEntity();
        user.setId(1L);

        //When
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        //Then
        assertAll("User Found",
                () -> {
                    assertDoesNotThrow(() -> userService.getUserById(user.getId()));
                    verify(userRepository, times(1)).findById(user.getId());
                });

    }

    @Test
    void getUserById_UserNotFound() {
        //Given
        Long userId = 2L;

        //When
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        //Then
        assertThrows(UserNotFoundException.class, () -> userService.getUserById(userId));

    }

    @Test
    void getAllUsers() {
        //Given
        int page = 0;
        int size = 10;

        List<UserEntity> userEntities = List.of(
                new UserEntity(1L, "FirstName1", "LastName1",
                        "UserName", "password1", "user1@gmail.com", "JobTitle1", Role.ADMIN),
                new UserEntity(2L, "FirstName2", "LastName2",
                        "UserName1", "password2", "user2@gmail.com", "JobTitle2", Role.ADMIN)
        );
        Page<UserEntity> mockUserPage = new PageImpl<>(userEntities);

        when(userRepository.findAll(PageRequest.of(page, size))).thenReturn(mockUserPage);

        //When
        Page<UserDto> result = userService.getAllUsers(page, size);

        //Then
        assertEquals(mockUserPage.getTotalElements(), result.getTotalElements());

    }

    @Test
    void updateUser_Success() {
        //Given
        UserEntity user = new UserEntity(1L, "FirstName", "LastName",
                "UserName", "password", "user@gmail.com", "JobTitle", Role.ADMIN);
        UpdateUserRequest request = new UpdateUserRequest("firstName", "lastName", "userName",
                "email", "jobTitle");
        userDetails = new User(user.getUsername(), user.getPassword(), Collections.emptyList());


        //When
        when(userRepository.findByUserName(userDetails.getUsername())).thenReturn(Optional.of(user));
        when(userRepository.save(any(UserEntity.class))).thenAnswer(invocation -> invocation.getArgument(0));


        //Then
        assertAll("User saved success",
                () -> {
                    assertDoesNotThrow(() -> userService.updateUser(request, userDetails));
                    verify(userRepository,times(1)).findByUserName(userDetails.getUsername());
                    verify(userRepository, times(1)).save(user);
                });
    }

    @Test
    void updateUser_UserNotFound() {
        // Given
        UpdateUserRequest request = new UpdateUserRequest("firstName", "lastName", "userName",
                "email", "jobTitle");
        userDetails = new User("UserName", "password", Collections.emptyList());

        // When
        when(userRepository.findByUserName(userDetails.getUsername())).thenReturn(Optional.empty());

        // Then
        assertThrows(UserNameNotFoundException.class, () -> userService.updateUser(request, userDetails));
    }

    @Test
    void deleteUser(){
        //Given
        String userName="userName";
        //When
        userService.deleteUser(userName);
        //Then
        verify(userRepository,times(1)).deleteByUserName(userName);
    }

}