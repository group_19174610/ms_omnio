package az.atl.ms_auth.service;

import az.atl.ms_auth.dao.entity.LoginTable;
import az.atl.ms_auth.dao.entity.UserEntity;
import az.atl.ms_auth.dao.repository.LoginTableRepository;
import az.atl.ms_auth.dao.repository.UserRepository;
import az.atl.ms_auth.exception.InvalidCredentialsException;
import az.atl.ms_auth.exception.UserNameExistsException;
import az.atl.ms_auth.exception.UserNameNotFoundException;
import az.atl.ms_auth.model.AuthenticationRequest;
import az.atl.ms_auth.model.RegisterRequest;
import az.atl.ms_auth.model.Role;
import az.atl.ms_auth.model.consts.ExceptionMessages;
import az.atl.ms_auth.service.impl.AuthenticationServiceImpl;
import az.atl.ms_auth.service.impl.JwtServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AuthenticationServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtServiceImpl jwtService;
    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private LoginTableRepository loginTableRepository;


    private AuthenticationServiceImpl authenticationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        authenticationService = new AuthenticationServiceImpl(userRepository, passwordEncoder, jwtService
                , authenticationManager, loginTableRepository);
    }


    @Test
    void register_Success() {
        //Given
        String userName = "userName1";
        RegisterRequest request = new RegisterRequest("firstName", "lastName", "userName", "password",
                "email@email.com", "jobTitle", Role.USER);

        UserEntity user = new UserEntity();
        user.setUserName(userName);

        //When
        when(userRepository.findByUserName(request.getUserName())).thenReturn(Optional.empty());
        when(userRepository.save(user)).thenReturn(user);

        //Then
        assertAll("Successfully registered",
                () -> {
                    assertDoesNotThrow(() -> authenticationService.register(request));
                    verify(userRepository, times(1)).findByUserName(request.getUserName());
                    verify(userRepository, times(1)).save(user);
                }
        );
    }


    @Test
    void register_UserNameExists() {
        //Given
        String userName = "userName";
        RegisterRequest request = new RegisterRequest("firstName", "lastName", "userName", "password",
                "email@email.com", "jobTitle", Role.USER);

        UserEntity user = new UserEntity();
        user.setUserName(userName);

        //When
        when(userRepository.findByUserName(userName)).thenReturn(Optional.of(user));

        //Then
        assertThrows(UserNameExistsException.class, () -> authenticationService.register(request));
    }

    @Test
    void authenticate_Success() {
        //Given
        String userName = "userName";
        Date date = new Date();
        AuthenticationRequest request = new AuthenticationRequest("userName", "password");

        UserEntity user = new UserEntity();
        user.setUserName(userName);

        LoginTable loginTable = new LoginTable();
        loginTable.setUserName(request.getUserName());
        loginTable.setPassword(request.getPassword());
        loginTable.setLoginDate(date);

        //When
        when(userRepository.findByUserName(request.getUserName())).thenReturn(Optional.of(user));
        when(jwtService.generateToken(user)).thenReturn("token");

        ArgumentCaptor<LoginTable> captor = ArgumentCaptor.forClass(LoginTable.class);

        //Then
        assertAll("Successfully authenticated",
                () -> {
                    assertDoesNotThrow(() -> authenticationService.authenticate(request));
                    verify(userRepository, times(1)).findByUserName(request.getUserName());
                    verify(jwtService, (times(1))).generateToken(user);
                    verify(loginTableRepository, times(1)).save(captor.capture());

                    LoginTable savedLogin = captor.getValue();
                    assertEquals(request.getUserName(), savedLogin.getUserName());
                    assertEquals(request.getPassword(), savedLogin.getPassword());

                }
        );
    }

    @Test
    void authenticate_InvalidCredentials() {
        //Given
        AuthenticationRequest request = new AuthenticationRequest("userName", "password");

        //When
        when(authenticationManager.authenticate(any(Authentication.class)))
                .thenThrow(new InvalidCredentialsException(ExceptionMessages.CREDENTIAL_EXCEPTION.getMessage()));

        //Then
        assertThrows(InvalidCredentialsException.class, () -> authenticationService.authenticate(request));
    }


    @Test
    void authenticate_UserNameNotFound() {
        //Given
        AuthenticationRequest request = new AuthenticationRequest("userName", "password");

        //When
        when(userRepository.findByUserName(request.getUserName())).thenReturn(Optional.empty());

        //Then
        assertThrows(UserNameNotFoundException.class,()->authenticationService.authenticate(request));
    }
}