package az.atl.ms_auth.controller;

import az.atl.ms_auth.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

class UserControllerTest {

    private final static String MAIN_URL = "/users";
    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }


    @Test
    void getUserById() {

    }

    @Test
    void getUserByUserName() {
    }

    @Test
    void getAllUsers() {
    }

    @Test
    void updatePassword() {
    }

    @Test
    void updateUser() {
    }

    @Test
    void deleteCurrentUser() {
    }
}