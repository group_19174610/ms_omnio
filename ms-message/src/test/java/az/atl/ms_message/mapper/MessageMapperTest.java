package az.atl.ms_message.mapper;

import az.atl.ms_message.dao.entity.MessageModel;
import az.atl.ms_message.dao.entity.UserEntity;
import az.atl.ms_message.model.consts.Channel;
import az.atl.ms_message.model.dto.AdminMessageDto;
import az.atl.ms_message.model.dto.MessageDto;
import az.atl.ms_message.model.dto.Role;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageMapperTest {

    @Test
    void testBuildMessageEntity() {
        //Given
        MessageDto messageDto = new MessageDto("senderName",
                "receiverName", "messageBody",
                true, Channel.CHANNEL_EMAIL, LocalDateTime.now());

        //When
        MessageModel result = MessageMapper.buildMessageEntity(messageDto);

        //Then
        assertAll(
                () -> assertEquals("messageBody", result.getMessageBody()),
                () -> assertEquals(Channel.CHANNEL_EMAIL, result.getChannel()),
                () -> assertEquals(messageDto.getSendDateTime(), result.getSendDateTime())
        );
    }

    @Test
    void testBuildMessageDto() {
        //Given
        UserEntity sender = new UserEntity(1L, "firstName", "lastName", "userName",
                "password", "email", "jobTitle", Role.ADMIN);
        UserEntity receiver = new UserEntity(2L, "firstName2", "lastName2", "userName2",
                "password2", "email2", "jobTitle2", Role.USER);
        MessageModel messageModel = new MessageModel(1L, "messageBody",
                sender, receiver, Channel.CHANNEL_EMAIL, LocalDateTime.now());

        //When
        MessageDto result = MessageMapper.buildMessageDto(messageModel);

        //Then
        assertAll(
                () -> assertEquals(sender.getUsername(), result.getSenderName()),
                () -> assertEquals(receiver.getUsername(), result.getReceiverName()),
                () -> assertEquals(messageModel.getMessageBody(), result.getMessageBody()),
                () -> assertEquals(messageModel.getChannel(), result.getChannel()),
                () -> assertEquals(messageModel.getSendDateTime(), result.getSendDateTime())
        );

    }


    @Test
    void buildAdminMessageDto() {
        //Given
        UserEntity sender = new UserEntity(1L, "firstName", "lastName", "userName",
                "password", "email", "jobTitle", Role.ADMIN);
        UserEntity receiver = new UserEntity(2L, "firstName2", "lastName2", "userName2",
                "password2", "email2", "jobTitle2", Role.USER);
        MessageModel messageModel = new MessageModel(1L, "messageBody",
                sender, receiver, Channel.CHANNEL_EMAIL, LocalDateTime.now());

        //When
        AdminMessageDto result = MessageMapper.buildAdminMessageDto(messageModel);

        //Then
        assertAll(
                () -> assertEquals(messageModel.getId(), result.getId()),
                () -> assertEquals(messageModel.getSender().getUsername(), result.getSenderName()),
                () -> assertEquals(messageModel.getReceiver().getUsername(), result.getReceiverName()),
                () -> assertEquals(messageModel.getMessageBody(), result.getMessageBody()),
                () -> assertEquals(messageModel.getChannel(), result.getChannel()),
                () -> assertEquals(messageModel.getSendDateTime(), result.getSendDateTime())
        );
    }


    @Test
    void buildMessageDtoList(){
        //Given
        UserEntity sender = new UserEntity(1L, "firstName", "lastName", "userName",
                "password", "email", "jobTitle", Role.ADMIN);
        UserEntity receiver = new UserEntity(2L, "firstName2", "lastName2", "userName2",
                "password2", "email2", "jobTitle2", Role.USER);
        List<MessageModel> messageModels=List.of(
                new MessageModel(1L, "messageBody",
                        sender, receiver, Channel.CHANNEL_EMAIL, LocalDateTime.now()),
                new MessageModel(2L, "messageBody2",
                        sender, receiver, Channel.CHANNEL_WHATSAPP, LocalDateTime.now())
        );

        //When
        List<MessageDto> result = MessageMapper.buildMessageDtoList(messageModels);

        //Then
        assertAll(
                () -> assertEquals(messageModels.get(0).getSender().getUsername(), result.get(0).getSenderName()),
                () -> assertEquals(messageModels.get(0).getReceiver().getUsername(), result.get(0).getReceiverName()),
                () -> assertEquals(messageModels.get(0).getMessageBody(), result.get(0).getMessageBody()),
                () -> assertEquals(messageModels.get(0).getChannel(), result.get(0).getChannel()),

                () -> assertEquals(messageModels.get(1).getSender().getUsername(), result.get(1).getSenderName()),
                () -> assertEquals(messageModels.get(1).getReceiver().getUsername(), result.get(1).getReceiverName()),
                () -> assertEquals(messageModels.get(1).getMessageBody(), result.get(1).getMessageBody()),
                () -> assertEquals(messageModels.get(1).getChannel(), result.get(1).getChannel())
        );
    }


}


